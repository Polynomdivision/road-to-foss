-- TODO: Change the hostname
-- Global Config
admins = { "admin@latinise.me" }

modules_enables = {
    "roster";
    "saslauth";
    "tls";
    "dialback";
    "disco";
    "offline";
    "posix";

    -- Mobile optimisations
    "csi";
    "throttle_presence";
    "checkcerts";

    -- Register
    "register_json";

    "blocklist";
    "vcard";
    "carbons";

    "register";
}

c2s_require_encryption = true
s2s_require_encryption = true
-- TODO?
s2s_secure_auth = true

authentication = "internal_hashed"

-- Hosts
-- TODO: MUC
VirtualHost "latinise.me"
    ssl = {
         key = "certs/latinise.me.key";
         certificate = "certs/latinise.me.crt";
    }
